//////

// Class for matching descriptors!!!!

///////


class descriptor_matcher{

public:
    descriptor_matcher(params *myparams):myparams_(myparams){

    }

    std::vector<cv::DMatch> match_descriptors(const cv::Mat &curr_desc, const cv::Mat &next_desc){

        std::vector<cv::DMatch> all_matches, good_matches;

        if(curr_desc.cols == 0 || next_desc.cols == 0){
            return good_matches;
        }

        std::string features = "BRISK";
        if(myparams_->feature_descriptor_ == features){
            matcher.match(curr_desc,next_desc,all_matches);
            // sort through all and find good matches

            //double max_dist = 0; double min_dist = 100;
            //-- Quick calculation of max and min distances between keypoints
            //for( int i = 0; i < curr_desc.rows; i++ )
            //{ double dist = all_matches[i].distance;
            //    if( dist < min_dist ) min_dist = dist;
            //    if( dist > max_dist ) max_dist = dist;
            //}

            //printf("-- Max dist : %f \n", max_dist );
            //printf("-- Min dist : %f \n", min_dist );

            //-- PS.- radiusMatch can also be used here.
            if(all_matches.size() > 0){
                for( int i = 0; i < curr_desc.rows; i++ )
                {
                    if( all_matches[i].distance <= myparams_->matching_th_)
                    {
                        good_matches.push_back(all_matches[i]);
                    }
                }
            }
        }

        features = "SURF";
        if(myparams_->feature_descriptor_ == features){

            matcher_.match(curr_desc,next_desc,all_matches);

            // sort through all and find good matches

            double max_dist = 0; double min_dist = 100;
            //-- Quick calculation of max and min distances between keypoints
            for( int i = 0; i < curr_desc.rows; i++ )
                { double dist = all_matches[i].distance;
                    if( dist < min_dist ) min_dist = dist;
                    if( dist > max_dist ) max_dist = dist;
                }

            //printf("-- Max dist : %f \n", max_dist );
            //printf("-- Min dist : %f \n", min_dist );

            //-- PS.- radiusMatch can also be used here.
            for( int i = 0; i < curr_desc.rows; i++ )
            {
                if( all_matches[i].distance <= 4*min_dist)
                {
                    good_matches.push_back(all_matches[i]);
                }
            }
        }

        //cout << " ----------------- Size of matches --------------" << endl;
        //cout << good_matches.size() <<endl;
        //cout << "--------------------------------------------------" <<endl;
        return good_matches;
    }

private:
    params *myparams_;
    cv::BruteForceMatcher <cv::Hamming> matcher;
    cv::FlannBasedMatcher matcher_;

};
