/////

/// A simple class for visualization!!

/////

class visualize_matches{
public:
    visualize_matches(params *myparams):myparams_(myparams){

    }

    ~visualize_matches();

    void display_matches(const cv::Mat &curr_img, const cv::Mat &next_img, const std::vector<cv::KeyPoint> curr_keypt,
                    const std::vector<cv::KeyPoint> next_keypt, std::vector< cv::DMatch > good_matches);

private:
    params *myparams_;
};


void visualize_matches::display_matches(const cv::Mat &curr_img, const cv::Mat &next_img, const std::vector<cv::KeyPoint> curr_keypt,
                                   const std::vector<cv::KeyPoint> next_keypt, std::vector< cv::DMatch > good_matches){

    //-- Draw only "good" matches
    cv::Mat img_matches;
    drawMatches( curr_img, curr_keypt, next_img, next_keypt,
                 good_matches, img_matches, cv::Scalar::all(-1), cv::Scalar::all(-1),
                 std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

    //-- Show detected matches
    imshow( "Good Matches", img_matches );
    cv::waitKey(200);
    //getchar();
}
