function []=write_file_malaga(directory,image_index,start_img_index,init_ts,num_hypothesis,hypo_index,prob_index)

%{

%%%%%%%%%% OLD CODE!!!!
fid = fopen(directory,'w');

%%% total hypothesis
%total_sum = 0;
total_sum = size(num_hypothesis,1);
%{
for i=1:size(num_hypothesis,1)
    if(num_hypothesis(i,1) >=20)
        total_sum = total_sum + 20;
    else if(num_hypothesis(i,1) < 20)
            total_sum = total_sum + num_hypothesis(i,1);            
        end
        
    end
end
%}

fprintf(fid,'%d',total_sum);
fprintf(fid,'\n');

start_ind_first_elem = start_img_index(1,1);
start_ind_second_elem = start_img_index(1,2);


for i=1:size(init_ts,1)
    [row col] = max(prob_index(i,:));
    for j=col:col
        first_elem = image_index(init_ts(i)+1,1);
        second_elem = image_index(init_ts(i)+1,2);

        first_diff = first_elem - start_ind_first_elem;
        second_diff = second_elem - start_ind_second_elem;
        
        actual_stamp = first_elem + second_elem/(10^6);
        diff_time_stamp = first_diff + second_diff/(10^6);
        
        hypo_first_elem = image_index(hypo_index(i,j)+1,1);
        hypo_second_elem = image_index(hypo_index(i,j)+1,2);
        
        hypo_first_diff = hypo_first_elem - start_ind_first_elem;
        hypo_second_diff = hypo_second_elem - start_ind_second_elem;

        actual_stamp_hypo = hypo_first_elem + hypo_second_elem/(10^6);
        time_stamp_hypo = hypo_first_diff + hypo_second_diff/(10^6);
        
        fprintf(fid,'%10.6f',diff_time_stamp);
        fprintf(fid,' ');
        fprintf(fid,'%10.6f',actual_stamp);
        fprintf(fid,' ');
        fprintf(fid,'%10.6f',time_stamp_hypo);       
        fprintf(fid,' ');
        fprintf(fid,'%10.6f',actual_stamp_hypo);       
        fprintf(fid,'\n');       
    end
end

fclose(fid);
%}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Operation of filtering matches

diff = 10;
status(size(num_hypothesis,1)) = 0;

start_ind_first_elem = start_img_index(1,1);
start_ind_second_elem = start_img_index(1,2);

for i=1:size(init_ts,1)
    if(i==1)
        status(i) = 1;
        continue;
    end
    
    if(i > 1)
        
        if(status(i-1) == 1)
            
            %%% extract difference time stamps of i
            [row col] = max(prob_index(i,:));
            j = col;
            
            first_elem = image_index(init_ts(i)+1,1);
            second_elem = image_index(init_ts(i)+1,2);

            first_diff = first_elem - start_ind_first_elem;
            second_diff = second_elem - start_ind_second_elem;
       
            ts_image = first_diff + second_diff/(10^6);

            hypo_first_elem = image_index(hypo_index(i,j)+1,1);
            hypo_second_elem = image_index(hypo_index(i,j)+1,2);

            hypo_first_diff = hypo_first_elem - start_ind_first_elem;
            hypo_second_diff = hypo_second_elem - start_ind_second_elem;

            ts_hypo = hypo_first_diff + hypo_second_diff/(10^6);
            
            %%% extract difference time stamps of i-1
            [row col] = max(prob_index(i-1,:));
            j = col;
            
            first_elem = image_index(init_ts(i-1)+1,1);
            second_elem = image_index(init_ts(i-1)+1,2);

            first_diff = first_elem - start_ind_first_elem;
            second_diff = second_elem - start_ind_second_elem;
       
            ts_prev_image = first_diff + second_diff/(10^6);

            hypo_first_elem = image_index(hypo_index(i-1,j)+1,1);
            hypo_second_elem = image_index(hypo_index(i-1,j)+1,2);

            hypo_first_diff = hypo_first_elem - start_ind_first_elem;
            hypo_second_diff = hypo_second_elem - start_ind_second_elem;

            ts_prev_hypo = hypo_first_diff + hypo_second_diff/(10^6);
            
        else

            [val_row val_col] = find(status(1:i-1) == 1);
            col = max(val_col);
            %%% extract difference time stamps of i
            [row2 col2] = max(prob_index(i,:));
            j = col2;
            
            first_elem = image_index(init_ts(i)+1,1);
            second_elem = image_index(init_ts(i)+1,2);

            first_diff = first_elem - start_ind_first_elem;
            second_diff = second_elem - start_ind_second_elem;
       
            ts_image = first_diff + second_diff/(10^6);

            hypo_first_elem = image_index(hypo_index(i,j)+1,1);
            hypo_second_elem = image_index(hypo_index(i,j)+1,2);

            hypo_first_diff = hypo_first_elem - start_ind_first_elem;
            hypo_second_diff = hypo_second_elem - start_ind_second_elem;

            ts_hypo = hypo_first_diff + hypo_second_diff/(10^6);
            
            %%% extract difference time stamps of i-1
            
            [row2 col2] = max(prob_index(col,:));
            j = col2;
            
            first_elem = image_index(init_ts(col)+1,1);
            second_elem = image_index(init_ts(col)+1,2);

            first_diff = first_elem - start_ind_first_elem;
            second_diff = second_elem - start_ind_second_elem;
       
            ts_prev_image = first_diff + second_diff/(10^6);

            hypo_first_elem = image_index(hypo_index(col,j)+1,1);
            hypo_second_elem = image_index(hypo_index(col,j)+1,2);

            hypo_first_diff = hypo_first_elem - start_ind_first_elem;
            hypo_second_diff = hypo_second_elem - start_ind_second_elem;

            ts_prev_hypo = hypo_first_diff + hypo_second_diff/(10^6);
        end
        
        if( (( abs(ts_image - ts_prev_image) < diff) && ( abs(ts_hypo - ts_prev_hypo) < diff)) ...
            || (( abs(ts_image - ts_prev_image) > diff) && ( abs(ts_hypo - ts_prev_hypo) > diff)) ...
            )
            status(i) = 1;
        end
    end
end


%status


total_hypo = find(status == 1);


fid = fopen(directory,'w');

fprintf(fid,'%d',size(total_hypo,2));
fprintf(fid,'\n');


for i=1:size(status,2)
    
    if(status(i) == 0)
        continue;
    end
    
    [row col] = max(prob_index(i,:));
    for j=col:col
        first_elem = image_index(init_ts(i)+1,1);
        second_elem = image_index(init_ts(i)+1,2);

        first_diff = first_elem - start_ind_first_elem;
        second_diff = second_elem - start_ind_second_elem;
        
        actual_stamp = first_elem + second_elem/(10^6);
        diff_time_stamp = first_diff + second_diff/(10^6);
        
        hypo_first_elem = image_index(hypo_index(i,j)+1,1);
        hypo_second_elem = image_index(hypo_index(i,j)+1,2);
        
        hypo_first_diff = hypo_first_elem - start_ind_first_elem;
        hypo_second_diff = hypo_second_elem - start_ind_second_elem;

        actual_stamp_hypo = hypo_first_elem + hypo_second_elem/(10^6);
        time_stamp_hypo = hypo_first_diff + hypo_second_diff/(10^6);
        
        fprintf(fid,'%10.6f',diff_time_stamp);
        fprintf(fid,' ');
        fprintf(fid,'%10.6f',actual_stamp);
        fprintf(fid,' ');
        fprintf(fid,'%10.6f',time_stamp_hypo);       
        fprintf(fid,' ');
        fprintf(fid,'%10.6f',actual_stamp_hypo);       
        fprintf(fid,'\n');       
    end
end

fclose(fid);

