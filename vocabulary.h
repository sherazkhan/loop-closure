///

// class to generate and update vocabulary!!!

///
#define EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET
#include <Eigen/Sparse>

using namespace Eigen;

typedef DynamicSparseMatrix<bool,ColMajor>::InnerIterator sparse_it;

class myvocabulary{

public:
    myvocabulary(params *myparams):myparams_(myparams),image_index(0),time_index_(0){
        inverse_index_zi = new DynamicSparseMatrix<bool,ColMajor>(10,1); // the initial size does not matter...it will be resized
        inverse_index_zi->setZero();
        inverse_index_iz = new DynamicSparseMatrix<bool,ColMajor>(1,10); // the initial size does not matter...it will be resized
        inverse_index_iz->setZero();

    }
    ~myvocabulary();

    // all imporatant methods!!!
    void cluster(std::vector < boost::dynamic_bitset<> > desc);
    void cluster(std::vector < boost::dynamic_bitset<> > desc, std::vector<correspondance> &mycorrespondance,
                 std::vector < boost::dynamic_bitset<> > &clustered_desc);
    void update_vocabulary();
    bool mean(std::vector < boost::dynamic_bitset<> > desc, boost::dynamic_bitset<> &mean);
    void update_inverted_index(int size);
    void assign_BOW_index();
    void update_visualwords();
    void update_occurances();

    // get the clustered desc!!
    const std::vector < boost::dynamic_bitset<> >* get_clustered_desc(){
        return &clustered_desc_;
    }

    // get size of clustered descriptors
    int get_size_clustered_desc(){
        return clustered_desc_.size();
    }

    // returns the vector of indexes corresponding to new words in the clustered descriptors!!!
    const std::vector <int>* get_new_words_index(){
        return &new_word_index_;
    }

    // get time index for vocabulary
    int get_time_index(){
        return time_index_;
    }

    // returns the number of new words!!!
    int get_num_new_words(){
        return new_word_index_.size();
    }

    // returns the vector of indexes corresponding to old words!!!
    const std::vector <int>* get_old_words_index(){
        return &old_word_index_;
    }

    // returns the number of old words!!!
    int get_num_old_words(){
        return old_word_index_.size();
    }

    // get size of vocabulary!!!
    int get_size_vocabulary(){
        return vocab_desc.size();
    }

    // returns a const pointer of a vector containing the vocabulary of visual words!!!
    std::vector< boost::dynamic_bitset<> >* get_visual_vocabulary(){
        return &vocab_desc;
    }

    // returns a const pointer of a vector containing num of occurances of each word
    std::vector<double>* get_visualword_occurances(){
        return &occurance;
    }

    // returns a const pointer the Z-I inverse index
    const DynamicSparseMatrix<bool,ColMajor>* return_inverse_index_zi(){
        return inverse_index_zi;
    }

    // returns a const pointer the I-Z inverse index
    const DynamicSparseMatrix<bool,ColMajor>* return_inverse_index_iz(){
        return inverse_index_iz;
    }

    void test_inverted_index();

    void update_variables();



protected:


private:
    /// tmp or working variables
    std::vector < boost::dynamic_bitset<> > clustered_desc_;
    std::vector <int> old_word_index_;
    std::vector <int> new_word_index_;

    /// actual variables
    int image_index;
    int time_index_;
    params *myparams_;
    std::vector < boost::dynamic_bitset<> > vocab_desc;
    std::vector <double> occurance;

    ///
    // mapping of visual words z (row index) and images (column index)
    ///
    static DynamicSparseMatrix<bool,ColMajor> *inverse_index_zi; // declare a dynamic sparse matrix of booleans!!!

    ///
    // mapping of images i (row index) and visual words z (column index)
    // redundant but it might help in liklehood calculations!!!
    ///

    static DynamicSparseMatrix<bool,ColMajor> *inverse_index_iz; // declare a dynamic sparse matrix of booleans!!!
};

////////////
//initialize static elements of the class...the vocabulary should be unique thats why the
// access fn passes a const pointer!!!
////////////

DynamicSparseMatrix<bool,ColMajor>* myvocabulary::inverse_index_zi = NULL;
DynamicSparseMatrix<bool,ColMajor>* myvocabulary::inverse_index_iz = NULL;


void myvocabulary::test_inverted_index(){


    cout << *inverse_index_zi << endl;
    cout << " Columns " << inverse_index_zi->cols() << " Rows " << inverse_index_zi->rows() << endl;

    // use insert if element does not exist
    // else use CoeffRef() func if element exists!!!

    //cout << inverse_index_zi->innerNonZeros(0) <<endl;
    //cout << inverse_index_zi->innerNonZeros(6) <<endl;
    //cout << inverse_index_zi->innerNonZeros(7) <<endl;
    //cout << inverse_index_zi->innerSize() <<endl;

    // resize and keep data!!
    inverse_index_zi->resizeAndKeepData(15,15);

    inverse_index_iz->resizeAndKeepData(5,20);

    inverse_index_zi->insert(14,7) = true;

    inverse_index_iz->insert(3,15) = true;

    //cout << *inverse_index_zi << endl;
    cout << " Columns " << inverse_index_zi->rows() << " Rows " << inverse_index_zi->rows() << endl;

    //cout << *inverse_index_iz << endl;
    cout << " Columns " << inverse_index_iz->cols() << " Rows " << inverse_index_iz->rows() << endl;

    for(sparse_it it(*inverse_index_zi,7);it;++it){
        cout << " Value " << it.value() << " Row number " << it.row() << " Column number " << it.col() << " Index in Matrix " << it.index() <<endl; // inner index, here it is equal to it.row()

    }

    for(sparse_it it(*inverse_index_iz,15);it;++it){
        cout << " Value " << it.value() << " Row number " << it.row() << " Column number " << it.col() << " Index in Matrix " << it.index() <<endl; // inner index, here it is equal to it.row()

    }


}

bool myvocabulary::mean(std::vector < boost::dynamic_bitset<> > desc, boost::dynamic_bitset<> &mymean){

    mymean.reset();

    if(desc.empty()) return false;

    const int N2 = desc.size() / 2;
    const int L = desc[0].size();

    std::vector<int> counters(L, 0);

    std::vector< boost::dynamic_bitset<> >::const_iterator it;
    for(it = desc.begin(); it != desc.end(); ++it)
    {
      boost::dynamic_bitset<> mydesc(myparams_->num_bits);
      mydesc = *it;

      for(int i = 0; i < L; ++i)
      {
        if(mydesc[i]) counters[i]++;
      }
    }

    for(int i = 0; i < L; ++i)
    {
      if(counters[i] > N2) {
          mymean.set(i);
      }
    }

    return true;
}



void myvocabulary::cluster(std::vector < boost::dynamic_bitset<> > desc){

    for(unsigned int i=0;i< desc.size();i++){
        std::vector <int> index;
        std::vector < boost::dynamic_bitset<> > tmp_desc;
        int min_distance = 10000; // any arbitary large value
        int tmp_index = 0;
        bool found = false;

        for(unsigned int j= i+1;j< desc.size();j++){

            boost::dynamic_bitset<> tmp = desc[i]^desc[j];
            if( (tmp.count() < myparams_->matching_th_) && (tmp.count() < min_distance)  ){
                //cout << " Similar words found!!!" <<endl;
                min_distance = tmp.count();
                tmp_index = j;
                found = true;
                //cout << " The correspondances are: " << i << " " << j <<endl;
            }

        }

        index.push_back(i);

        if(found){
            index.push_back(tmp_index);
        }

        //confirm if this part actually deletes the right index descriptor!!!
        for(unsigned int k=0; k<index.size();k++){
            tmp_desc.push_back(desc[index[k]]);

            if(index[k] > i){
                desc.erase(desc.begin() + index[k]);
            }
        }

        if(index.size() > 1){

            //find mean of descriptors!!!
            boost::dynamic_bitset<> mymean(myparams_->num_bits);
            bool calculated = mean(tmp_desc,mymean);

            if(calculated)
                clustered_desc_.push_back(mymean);

        }
        else if(index.size() == 1){
            // if no corresponding cluster, save it!!!
            clustered_desc_.push_back(tmp_desc[tmp_desc.size()-1]);
        }

        //clear list of temp variables...actually not required..but who gives a shit!!

        tmp_desc.clear();
        index.clear();

    }

}

void myvocabulary::cluster(std::vector < boost::dynamic_bitset<> > desc, std::vector<correspondance> &mycorrespondance,std::vector < boost::dynamic_bitset<> > &clustered_desc){

    for(unsigned int i=0;i< desc.size();i++){
        std::vector <int> index;
        std::vector < boost::dynamic_bitset<> > tmp_desc;

        for(unsigned int j= i+1;j< desc.size();j++){

            boost::dynamic_bitset<> tmp = desc[i]^desc[j];
            if(tmp.count() < myparams_->matching_th_){
                //cout << " Similar words found!!!" <<endl;
                index.push_back(j);

                correspondance tmp_corr;
                tmp_corr.index1 = i;
                tmp_corr.index2 = j;
                mycorrespondance.push_back(tmp_corr);

            }

        }

        index.push_back(i);

        for(unsigned int k=0; k<index.size();k++){
            tmp_desc.push_back(desc[index[k]]);

            if(index[k] > i){
                desc.erase(desc.begin() + index[k]);
            }
        }

        if(index.size() > 1){

            //find mean of descriptors!!!
            boost::dynamic_bitset<> mymean(myparams_->num_bits);
            bool calculated = mean(tmp_desc,mymean);

            if(calculated)
                clustered_desc.push_back(mymean);

        }
        else if(index.size() == 1){
            // if no corresponding cluster, save it!!!
            clustered_desc.push_back(tmp_desc[tmp_desc.size()-1]);
        }

        //clear list of temp variables...actually not required..but who gives a shit!!

        tmp_desc.clear();
        index.clear();

    }

}


void myvocabulary::assign_BOW_index(){
    //assign each visual word an index for the vocab!!

    if(time_index_ == 0){
        // all new words
        for(unsigned int i=0; i < clustered_desc_.size() ;i++){
            // should not be done !!!...wait after loop closure for this!!!
            // update vocabulary as its the first iteration!!!
            //vocab_desc.push_back(clustered_desc_[i]);
            new_word_index_.push_back(i);
        }
    }
    else if(time_index_ > 0){ // check code after 1st iteration!!!
        // check if a word is already present in vocabulary!!
        std::vector < boost::dynamic_bitset<> >::iterator it1,it2;

        //quite expensive for large vocabularies!!...look into vocabulary trees
        for(it1 = clustered_desc_.begin(); it1 < clustered_desc_.end(); it1++){
            bool old_word = false;
            for(it2 = vocab_desc.begin(); it2 < vocab_desc.end(); it2++){
                boost::dynamic_bitset<> tmp(myparams_->num_bits);
                tmp = (*it1) ^ (*it2);

                if(tmp.count() < myparams_->matching_th_){
                    //cout << " Word already exists " << endl;
                    old_word = true;
                    //store index here!!!
                    old_word_index_.push_back(it2 - vocab_desc.begin());
                    break;
                }
            }

            if( (!old_word) && (vocab_desc.size() < myparams_->max_vocab_size_) ){
                // new word.. store index!!!
                new_word_index_.push_back(it1-clustered_desc_.begin());
            }

        }

    }

    return;

}

void myvocabulary::update_visualwords(){

    // update vocabulary with new words!!!
    for(unsigned int i=0; i< new_word_index_.size();i++){
        vocab_desc.push_back(clustered_desc_[new_word_index_[i]]);
    }

}


void myvocabulary::update_occurances(){

    if(time_index_ == 0 && old_word_index_.size() == 0){
        occurance.resize(new_word_index_.size(),1);
    }
    else if(time_index_ > 0){

        // update occurance of old words!!!
        for(unsigned int i=0;i < old_word_index_.size();i++){
            occurance[old_word_index_[i]]++;
        }

        // add new words
        occurance.resize(occurance.size() + new_word_index_.size(),1);
    }

    return;
}


void myvocabulary::update_inverted_index(int size){
    //update stupid index here!!!

    if(time_index_ == 0 && old_word_index_.size() == 0){
        // old word index should hopefully--- be zero!!!
        // all new words so initialize
        inverse_index_zi->resize(clustered_desc_.size(),time_index_ + 1);
        inverse_index_iz->resize(time_index_ + 1,clustered_desc_.size());

        //cout << " Columns " << inverse_index_zi->cols() << " Rows " << inverse_index_zi->rows() << endl;

        // set all indexes as true!!!
        for(unsigned i=0;i< clustered_desc_.size();i++){
            inverse_index_zi->insert(i,time_index_) = true;
            inverse_index_iz->insert(time_index_,i) = true;
        }

    }
    else if(time_index_ > 0){ // check code after 1st iteration!!!

        int new_words = new_word_index_.size();

        int resize_rows = size + new_words;
        int resize_cols = size + new_words;

        // resize the vocabulary
        inverse_index_zi->resizeAndKeepData(resize_rows,time_index_ + 1);
        inverse_index_iz->resizeAndKeepData(time_index_ + 1,resize_cols);

        // insert old words
        for(unsigned int i=0;i< old_word_index_.size();i++){
            // extract index for the row (zi inverse index) and cols (iz inverse index)
            int row_index = old_word_index_[i];
            int col_index = old_word_index_[i];

            // insert elements at those indexes
            inverse_index_zi->insert(row_index,time_index_) = true;
            inverse_index_iz->insert(time_index_,col_index) = true;
        }

        // extract index for the row (zi inverse index) and cols (iz inverse index)
        int init_row_index = size;
        int init_col_index = size;

        // insert new words
        for(unsigned int i=0;i < new_word_index_.size();i++){
            // insert elements at those indexes
            inverse_index_zi->insert(init_row_index+i,time_index_) = true;
            inverse_index_iz->insert(time_index_,init_col_index+i) = true;
        }

    }

    return;

}

//////
// TODO!!!

// Add checks that clustering of descriptors takes place first
// then assignment to indexes in the vocabulary
// and the corresponding loop closure check
// and finally the update of vocabulary!!

// if any one changes this order --  things are going to be really screwed!!!!

/////

void myvocabulary::update_vocabulary(){

    int prev_vocab_size = vocab_desc.size();
    // update visual words!!!
    update_visualwords();

   // update the presence of each visual word!!!
    update_occurances();

    // update the sparse inverted index using the clustered indexes!!!
    update_inverted_index(prev_vocab_size);

    // clear all tmp variables of this iteration!!!
    //clustered_desc_.clear();
    //old_word_index_.clear();
    //new_word_index_.clear();

    //increment counter for time index
    //time_index_++;
}

void myvocabulary::update_variables(){

    // clear all tmp variables of this iteration!!!
    clustered_desc_.clear();
    old_word_index_.clear();
    new_word_index_.clear();

    //increment counter for time index
    time_index_++;
}


