////

// read images from files

////

// methods for getinput class!!!

bool get_input::read_curr_image(cv::Mat &curr_img){

    if(myparams->dataset_ == "Ford_campus"){
        ///The file to read from.
        char directory[100];
        std::stringstream sstr;
        sstr << myparams->curr_index_img_;
        std::string str1 = sstr.str();

        strcpy (directory, myparams->directory);

        if(myparams->curr_index_img_ < 10){
            strcat(directory,"image");
            strcat(directory,"000");
            strcat(directory,str1.c_str());
        }else if(myparams->curr_index_img_ >= 10 && myparams->curr_index_img_ < 100){
            strcat(directory,"image");
            strcat(directory,"00");
            strcat(directory,str1.c_str());
        }else if(myparams->curr_index_img_ >= 100 && myparams->curr_index_img_ < 1000){
            strcat(directory,"image");
            strcat(directory,"0");
            strcat(directory,str1.c_str());
        }else{
            strcat(directory,"image");
            strcat(directory,str1.c_str());
        }

        //strcat(directory, myparams->index_);
        strcat (directory, myparams->img_extension_);
        std::cout << directory <<std::endl;

        curr_img = cv::imread(directory,CV_LOAD_IMAGE_GRAYSCALE);

        int num_rows = curr_img.rows;

        ///extract middle image!!!
        int start_rows = (num_rows/5)*2;
        int end_rows = (num_rows/5)*3;

        int total_rows = end_rows - start_rows;

        cv::Mat tmp_curr = cv::Mat::zeros(total_rows, curr_img.cols,curr_img.type());

        int count = 0;
        for(unsigned int j=start_rows;j<end_rows;j++){
            curr_img.row(j).copyTo(tmp_curr.row(count));
            count++;
        }

        tmp_curr.copyTo(curr_img);
        cv::transpose(curr_img,curr_img);

        if(curr_img.rows > 0 && curr_img.cols > 0){
            myparams->curr_index_img_ += 2;

            if( myparams->curr_index_img_ >= myparams->end_index_img_ ){
                myparams->end_input = true;
            }
            return true;
        }
        else{
            return false;
        }
    }else if(myparams->dataset_ == "Lip6_outdoor"){
        ///The file to read from.
        char directory[100];
        std::stringstream sstr;
        sstr << myparams->curr_index_img_;
        std::string str1 = sstr.str();

        strcpy (directory, myparams->directory);

        if(myparams->curr_index_img_ < 10){
            strcat(directory,"000");
            strcat(directory,str1.c_str());
        }else if(myparams->curr_index_img_ >= 10 && myparams->curr_index_img_ < 100){
            strcat(directory,"00");
            strcat(directory,str1.c_str());
        }else if(myparams->curr_index_img_ >= 100 && myparams->curr_index_img_ < 1000){
            strcat(directory,"0");
            strcat(directory,str1.c_str());
        }else{
            strcat(directory,str1.c_str());
        }

        //strcat(directory, myparams->index_);
        strcat (directory, myparams->img_extension_);
        //std::cout << directory <<std::endl;

        curr_img = cv::imread(directory,CV_LOAD_IMAGE_GRAYSCALE);

        if(curr_img.rows > 0 && curr_img.cols > 0){
            myparams->curr_index_img_ += 1;

            if( myparams->curr_index_img_ >= myparams->end_index_img_ ){
                myparams->end_input = true;
            }
            return true;
        }
        else{
            return false;
        }
    }else if(myparams->dataset_ == "Lip6"){
        ///The file to read from.
        char directory[100];
        std::stringstream sstr;
        sstr << myparams->curr_index_img_;
        std::string str1 = sstr.str();

        strcpy (directory, myparams->directory);

        if(myparams->curr_index_img_ < 10){
            strcat(directory,"lip6kennedy_bigdoubleloop_");
            strcat(directory,"00000");
            strcat(directory,str1.c_str());
        }else if(myparams->curr_index_img_ >= 10 && myparams->curr_index_img_ < 100){
            strcat(directory,"lip6kennedy_bigdoubleloop_");
            strcat(directory,"0000");
            strcat(directory,str1.c_str());
        }else{
            strcat(directory,"lip6kennedy_bigdoubleloop_");
            strcat(directory,"000");
            strcat(directory,str1.c_str());
        }

        //strcat(directory, myparams->index_);
        strcat (directory, myparams->img_extension_);
        //std::cout << directory <<std::endl;

        curr_img = cv::imread(directory,CV_LOAD_IMAGE_GRAYSCALE);

        if(curr_img.rows > 0 && curr_img.cols > 0){
            myparams->curr_index_img_ += 1;

            if( myparams->curr_index_img_ >= myparams->end_index_img_ ){
                myparams->end_input = true;
            }
            return true;
        }
        else{
            return false;
        }
    }else if(myparams->dataset_ == "CityCentre"){
        ///The file to read from.
        char directory[100];
        std::stringstream sstr;
        sstr << myparams->curr_index_img_;
        std::string str1 = sstr.str();

        strcpy (directory, myparams->directory);

        if(myparams->curr_index_img_ < 10){
            strcat(directory,"000");
            strcat(directory,str1.c_str());
        }else if(myparams->curr_index_img_ >= 10 && myparams->curr_index_img_ < 100){
            strcat(directory,"00");
            strcat(directory,str1.c_str());
        }else if(myparams->curr_index_img_ >= 100 && myparams->curr_index_img_ < 1000){
            strcat(directory,"0");
            strcat(directory,str1.c_str());
        }else if(myparams->curr_index_img_ >= 1000){
            strcat(directory,str1.c_str());
        }

        //strcat(directory, myparams->index_);
        strcat (directory, myparams->img_extension_);
        //std::cout << directory <<std::endl;

        curr_img = cv::imread(directory,CV_LOAD_IMAGE_GRAYSCALE);

        if(curr_img.rows > 0 && curr_img.cols > 0){
            myparams->curr_index_img_ += 2;

            if( myparams->curr_index_img_ >= myparams->end_index_img_ ){
                myparams->end_input = true;
            }
            return true;
        }
        else{
            return false;
        }
    }else if(myparams->dataset_ == "NewCollege"){

        // INPUT FROM NEW COLLEGE DATA SET WITH FRONTAL CAMERAS

        ///The file to read from.
        char directory[100];
        std::stringstream sstr;
        sstr << myparams->curr_index_img_;
        std::string str1 = sstr.str();

        strcpy (directory, myparams->directory);

        strcat(directory,str1.c_str());
        //strcat(directory, myparams->index_);
        strcat (directory, myparams->img_extension_);
        //std::cout << directory <<std::endl;

        curr_img = cv::imread(directory,CV_LOAD_IMAGE_GRAYSCALE);

        if(curr_img.rows > 0 && curr_img.cols > 0){
            myparams->curr_index_img_++;

            if( myparams->curr_index_img_ >= myparams->end_index_img_ ){
                myparams->end_input = true;
            }
            return true;
        }
        else{
            return false;
        }

    }else if(myparams->dataset_ == "Malaga"){

        char directory[100];

        if (file_iterator::is_directory(readdir_))
        {
          if(directory_it_ != directory_endit_)
          {
              file_iterator::path tmp = directory_it_->path().filename();

              if(boost::algorithm::contains(tmp.string(),myparams->index_)){
                  //cout << directory_it_->path().filename() << endl;
                  std::string mytmp = tmp.string();
                  strcpy(directory, myparams->directory);
                  strcat(directory,mytmp.c_str());
                  //cout << directory <<endl;
                  curr_img = cv::imread(directory,CV_LOAD_IMAGE_GRAYSCALE);

                  // parse current index
                  boost::cmatch matches;
                  if (boost::regex_match(mytmp.c_str(), matches, myparams->format_))
                  {
                      std::string tmp_index1(matches[2].first,matches[2].second);
                      std::string tmp_index2(matches[3].first,matches[3].second);

                      char start_index[100];
                      strcpy(start_index,tmp_index1.c_str());

                      char start_index2[100];
                      strcpy(start_index2,tmp_index2.c_str());

                      // set value for current image index too
                      myparams->curr_index_img_ = atoi(start_index);
                      myparams->curr_index_img2_ = atoi(start_index2);


                  }else{
                      cout << " Boost format does not match " <<endl;
                  }

              }else{
                  myparams->end_input = true;

              }

            ++directory_it_;
          }
          return true;
        }
        else{
            return false;
        }


    }
}


bool get_input::read_consecutive_images(cv::Mat &curr_img, cv::Mat &next_img){

    if(myparams->dataset_ == "Ford_campus"){

        // INPUT FROM FORD CAMPUS!!!

        ///The file to read from.
        char directory_curr[100],directory_next[100];

        //
        std::stringstream sstr;
        sstr << myparams->curr_index_img_;
        std::string str_curr = sstr.str();

        //clear stream
        sstr.str(std::string());

        //read next file index
        int temp = myparams->curr_index_img_+1;
        sstr << temp;
        std::string str_next = sstr.str();

        // check this again!!!!
        strcpy(directory_curr, myparams->directory);
        strcpy(directory_next, myparams->directory);

        if(myparams->curr_index_img_ < 10){
            strcat(directory_curr,"image");
            strcat(directory_curr,"000");

            strcat(directory_next,"image");
            strcat(directory_next,"000");

            //add img index
            strcat(directory_curr,str_curr.c_str());
            strcat(directory_next,str_next.c_str());
        }else if(myparams->curr_index_img_ >= 10 && myparams->curr_index_img_ < 100){
            strcat(directory_curr,"image");
            strcat(directory_curr,"00");

            strcat(directory_next,"image");
            strcat(directory_next,"00");

            //add img index
            strcat(directory_curr,str_curr.c_str());
            strcat(directory_next,str_next.c_str());
        }


        // add extension and index!!!
        //strcat(directory_curr, myparams->index_);
        strcat (directory_curr, myparams->img_extension_);
        //std::cout << " Current image directory: "<< directory_curr <<std::endl;

        //strcat(directory_next, myparams->index_);
        strcat (directory_next, myparams->img_extension_);
        //std::cout << " Next image directory: " << directory_next <<std::endl;

        curr_img = cv::imread(directory_curr,CV_LOAD_IMAGE_GRAYSCALE);
        next_img = cv::imread(directory_next,CV_LOAD_IMAGE_GRAYSCALE);

        int num_rows = curr_img.rows;

        ///extract middle image!!!
        int start_rows = (num_rows/5)*2;
        int end_rows = (num_rows/5)*3;

        int total_rows = end_rows - start_rows;

        cv::Mat tmp_curr = cv::Mat::zeros(total_rows, curr_img.cols,curr_img.type());
        cv::Mat tmp_next = cv::Mat::zeros(total_rows, next_img.cols,next_img.type());

        int count = 0;
        for(unsigned int j=start_rows;j<end_rows;j++){
            next_img.row(j).copyTo(tmp_next.row(count));
            curr_img.row(j).copyTo(tmp_curr.row(count));
            count++;
        }

        tmp_next.copyTo(next_img);
        tmp_curr.copyTo(curr_img);

        cv::transpose(next_img,next_img);
        cv::transpose(curr_img,curr_img);

        //cv::equalizeHist( curr_img, curr_img );
        //cv::equalizeHist( next_img, next_img );

        if(curr_img.rows > 0 && curr_img.cols > 0 && next_img.rows > 0 && next_img.cols > 0){
            myparams->curr_index_img_ += 2;

            if( myparams->curr_index_img_ >= myparams->end_index_img_ ){
                myparams->end_input = true;
            }
            return true;
        }
        else{
            return false;
        }

    }else if(myparams->dataset_ == "Lip6_outdoor"){
        // INPUT FROM CITY CENTER!!!

        ///The file to read from.
        char directory_curr[100],directory_next[100];

        //
        std::stringstream sstr;
        sstr << myparams->curr_index_img_;
        std::string str_curr = sstr.str();

        //clear stream
        sstr.str(std::string());

        //read next file index
        int temp = myparams->curr_index_img_+1;
        sstr << temp;
        std::string str_next = sstr.str();

        // check this again!!!!
        strcpy(directory_curr, myparams->directory);
        strcpy(directory_next, myparams->directory);

        if(myparams->curr_index_img_ < 10){
            strcat(directory_curr,"000");
            strcat(directory_next,"000");

            //add img index
            strcat(directory_curr,str_curr.c_str());
            strcat(directory_next,str_next.c_str());
        }

        // add extension and index!!!
        //strcat(directory_curr, myparams->index_);
        strcat (directory_curr, myparams->img_extension_);
        //std::cout << " Current image directory: "<< directory_curr <<std::endl;

        //strcat(directory_next, myparams->index_);
        strcat (directory_next, myparams->img_extension_);
        //std::cout << " Next image directory: " << directory_next <<std::endl;

        curr_img = cv::imread(directory_curr,CV_LOAD_IMAGE_GRAYSCALE);
        next_img = cv::imread(directory_next,CV_LOAD_IMAGE_GRAYSCALE);

        //cv::equalizeHist( curr_img, curr_img );
        //cv::equalizeHist( next_img, next_img );

        if(curr_img.rows > 0 && curr_img.cols > 0 && next_img.rows > 0 && next_img.cols > 0){
            myparams->curr_index_img_ += 2;

            if( myparams->curr_index_img_ >= myparams->end_index_img_ ){
                myparams->end_input = true;
            }
            return true;
        }
        else{
            return false;
        }
    }else if(myparams->dataset_ == "Lip6"){
        // INPUT FROM CITY CENTER!!!

        ///The file to read from.
        char directory_curr[100],directory_next[100];

        //
        std::stringstream sstr;
        sstr << myparams->curr_index_img_;
        std::string str_curr = sstr.str();

        //clear stream
        sstr.str(std::string());

        //read next file index
        int temp = myparams->curr_index_img_+1;
        sstr << temp;
        std::string str_next = sstr.str();

        // check this again!!!!
        strcpy(directory_curr, myparams->directory);
        strcpy(directory_next, myparams->directory);

        if(myparams->curr_index_img_ < 10){
            strcat(directory_curr,"lip6kennedy_bigdoubleloop_");
            strcat(directory_curr,"00000");
            strcat(directory_next,"lip6kennedy_bigdoubleloop_");
            strcat(directory_next,"00000");

            //add img index
            strcat(directory_curr,str_curr.c_str());
            strcat(directory_next,str_next.c_str());
        }

        // add extension and index!!!
        //strcat(directory_curr, myparams->index_);
        strcat (directory_curr, myparams->img_extension_);
        //std::cout << " Current image directory: "<< directory_curr <<std::endl;

        //strcat(directory_next, myparams->index_);
        strcat (directory_next, myparams->img_extension_);
        //std::cout << " Next image directory: " << directory_next <<std::endl;

        curr_img = cv::imread(directory_curr,CV_LOAD_IMAGE_GRAYSCALE);
        next_img = cv::imread(directory_next,CV_LOAD_IMAGE_GRAYSCALE);

        //cv::equalizeHist( curr_img, curr_img );
        //cv::equalizeHist( next_img, next_img );

        if(curr_img.rows > 0 && curr_img.cols > 0 && next_img.rows > 0 && next_img.cols > 0){
            myparams->curr_index_img_ += 2;

            if( myparams->curr_index_img_ >= myparams->end_index_img_ ){
                myparams->end_input = true;
            }
            return true;
        }
        else{
            return false;
        }
    }else if(myparams->dataset_ == "CityCentre"){
        // INPUT FROM CITY CENTER!!!

        ///The file to read from.
        char directory_curr[100],directory_next[100];

        //
        std::stringstream sstr;
        sstr << myparams->curr_index_img_;
        std::string str_curr = sstr.str();

        //clear stream
        sstr.str(std::string());

        //read next file index
        int temp = myparams->curr_index_img_+2;
        sstr << temp;
        std::string str_next = sstr.str();

        // check this again!!!!
        strcpy(directory_curr, myparams->directory);
        strcpy(directory_next, myparams->directory);

        if(myparams->curr_index_img_ < 10){
            strcat(directory_curr,"000");
            strcat(directory_next,"000");

            //add img index
            strcat(directory_curr,str_curr.c_str());
            strcat(directory_next,str_next.c_str());
        }

        // add extension and index!!!
        //strcat(directory_curr, myparams->index_);
        strcat (directory_curr, myparams->img_extension_);
        //std::cout << " Current image directory: "<< directory_curr <<std::endl;

        //strcat(directory_next, myparams->index_);
        strcat (directory_next, myparams->img_extension_);
        //std::cout << " Next image directory: " << directory_next <<std::endl;

        curr_img = cv::imread(directory_curr,CV_LOAD_IMAGE_GRAYSCALE);
        next_img = cv::imread(directory_next,CV_LOAD_IMAGE_GRAYSCALE);

        //cv::equalizeHist( curr_img, curr_img );
        //cv::equalizeHist( next_img, next_img );

        if(curr_img.rows > 0 && curr_img.cols > 0 && next_img.rows > 0 && next_img.cols > 0){
            myparams->curr_index_img_ += 2;

            if( myparams->curr_index_img_ >= myparams->end_index_img_ ){
                myparams->end_input = true;
            }
            return true;
        }
        else{
            return false;
        }
    }else if(myparams->dataset_ == "NewCollege"){

        // Input from NEW COLLEGE DATA SET with FRONTAL CAMERAS

        ///The file to read from.
        char directory_curr[100],directory_next[100];

        //
        std::stringstream sstr;
        sstr << myparams->curr_index_img_;
        std::string str_curr = sstr.str();

        //clear stream
        sstr.str(std::string());

        //read next file index
        int temp = myparams->curr_index_img_+1;
        sstr << temp;
        std::string str_next = sstr.str();

        // check this again!!!!
        strcpy(directory_curr, myparams->directory);
        strcpy(directory_next, myparams->directory);

        //add img index
        strcat(directory_curr,str_curr.c_str());
        strcat(directory_next,str_next.c_str());

        // add extension and index!!!
        //strcat(directory_curr, myparams->index_);
        strcat (directory_curr, myparams->img_extension_);
        //std::cout << " Current image directory: "<< directory_curr <<std::endl;

        //strcat(directory_next, myparams->index_);
        strcat (directory_next, myparams->img_extension_);
        //std::cout << " Next image directory: " << directory_next <<std::endl;

        curr_img = cv::imread(directory_curr,CV_LOAD_IMAGE_GRAYSCALE);
        next_img = cv::imread(directory_next,CV_LOAD_IMAGE_GRAYSCALE);

        if(curr_img.rows > 0 && curr_img.cols > 0 && next_img.rows > 0 && next_img.cols > 0){
            myparams->curr_index_img_ += 2;

            if( myparams->curr_index_img_ >= myparams->end_index_img_ ){
                myparams->end_input = true;
            }
            return true;
        }
        else{
            return false;
        }

    }else if(myparams->dataset_ == "Malaga"){

        char directory_curr[100], directory_next[100];
        if (file_iterator::is_directory(readdir_))
        {
            if(directory_it_ != directory_endit_)
            {
                file_iterator::path tmp = directory_it_->path().filename();
                if(boost::algorithm::contains(tmp.string(),myparams->index_)){
                    //cout << directory_it_->path().filename() << endl;
                    std::string mytmp = tmp.string();
                    strcpy(directory_curr, myparams->directory);
                    strcat(directory_curr,mytmp.c_str());
                    //cout << directory_curr <<endl;
                    curr_img = cv::imread(directory_curr,CV_LOAD_IMAGE_GRAYSCALE);

                    // parse current index
                    boost::cmatch matches;
                    if (boost::regex_match(mytmp.c_str(), matches, myparams->format_))
                    {
                        std::string tmp_index1(matches[2].first,matches[2].second);
                        std::string tmp_index2(matches[3].first,matches[3].second);

                        char start_index[100];
                        strcpy(start_index,tmp_index1.c_str());
                        // set start index
                        myparams->start_index_img_ = atoi(start_index);

                        char start_index2[100];
                        strcpy(start_index2,tmp_index2.c_str());
                        // set start index
                        myparams->start_index_img2_ = atoi(start_index2);

                        // set value for current image index too
                        myparams->curr_index_img_ = myparams->start_index_img_;
                        myparams->curr_index_img2_ = myparams->start_index_img2_;


                    }else{
                        cout << " Boost format does not match " <<endl;
                    }

                }

                ++directory_it_;
            }

            if(directory_it_ != directory_endit_)
            {
                file_iterator::path tmp = directory_it_->path().filename();
                if(boost::algorithm::contains(tmp.string(),myparams->index_)){
                    //cout << directory_it_->path().filename() << endl;
                    std::string mytmp = tmp.string();
                    strcpy(directory_next, myparams->directory);
                    strcat(directory_next,mytmp.c_str());
                    //cout << directory_next <<endl;
                    next_img = cv::imread(directory_next,CV_LOAD_IMAGE_GRAYSCALE);
                }

                //++directory_it_;
            }
            return true;
        }
        else{
            return false;
        }
    }

}

void get_input::write_params(){

    cout << endl;
    cout  << "-------------------Input file parameters----------------------------" <<endl;
    cout << " Parameters of the PROGRAM " <<endl;
    cout << " Starting image index: " << myparams->start_index_img_ <<endl;
    cout << " Current image index: " << myparams->curr_index_img_ <<endl;
    cout << " Final image index: " << myparams->end_index_img_ << endl;
    cout << " Directory to read from: " << myparams->directory <<endl <<endl;

}
