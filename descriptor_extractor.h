///

// Descriptor class with methods for conversion between
// cv::Mat and bit::set !!

///
#include "parameters.h"
#include <vector>
#include <boost/dynamic_bitset.hpp>        // boost::bitset

class descriptor_extractor{

public:
    descriptor_extractor(params* myparams):myparams_(myparams),scale_invariance(myparams_->scale_invariance),rotation_invariance(myparams_->rotation_invariance){
        std::string feature_type = "BRISK";
        if(feature_type == myparams_->feature_descriptor_){
            descriptorExtractor = new cv::BriskDescriptorExtractor(scale_invariance,rotation_invariance);
        }
        feature_type = "SURF";
        if(feature_type == myparams_->feature_descriptor_){
            cout << " Using SURF descriptor " <<endl;
            descriptorExtractor = new cv::SurfDescriptorExtractor();
        }

    }

    double timediff(const timeval& start, const timeval& stop){
      return (stop.tv_sec - start.tv_sec) + 1.0e-6 *(stop.tv_usec - start.tv_usec);
    }

    double extract_descriptors(const cv::Mat &img, std::vector<cv::KeyPoint> &keypoints, cv::Mat &desc);
    vector< boost::dynamic_bitset<> > extract_descriptors(const cv::Mat &img, std::vector<cv::KeyPoint> &keypoints);
    vector< boost::dynamic_bitset<> > extract_bitset_descriptors(const cv::Mat desc, std::vector <cv::DMatch> matches);
    void extract_bitset_descriptors_indices(const cv::Mat desc, std::vector <cv::DMatch> matches, std::vector<int> &indices,vector< boost::dynamic_bitset<> > &mydesc);


private:
    cv::Ptr<cv::DescriptorExtractor> descriptorExtractor;
    params * myparams_;
    bool scale_invariance;
    bool rotation_invariance;
    timeval start;
    timeval stop;
};

double descriptor_extractor::extract_descriptors(const cv::Mat &img, std::vector<cv::KeyPoint> &keypoints, cv::Mat &desc){
    gettimeofday(&start, NULL);  // stop timer
    descriptorExtractor->compute(img,keypoints,desc);
    gettimeofday(&stop, NULL);  // stop timer
    double extraction_time = timediff(start, stop);
    cout << " --------------- " <<endl;
    cout << " Time to extract descriptors " << extraction_time <<endl;
    cout << " --------------- " <<endl;

    return extraction_time;
}

vector< boost::dynamic_bitset<> > descriptor_extractor::extract_descriptors(const cv::Mat &img, std::vector<cv::KeyPoint> &keypoints){
    cv::Mat desc;
    descriptorExtractor->compute(img,keypoints,desc);

    boost::dynamic_bitset<> mybit(myparams_->num_bits);
    vector < boost::dynamic_bitset<> > mydesc;
    for(unsigned int i=0;i < desc.rows;i++){
        for(unsigned int j=0; j < desc.cols; j++){
            int tmp = (unsigned long int)desc.at<unsigned char>(i,j);
            boost::dynamic_bitset<> mybit_tmp(myparams_->num_bits,tmp);
            //shit bits!!!
            mybit = mybit << 8;
            //store new val!!
            mybit = mybit | mybit_tmp;
        }

        mydesc.push_back(mybit);
        mybit.reset();
    }

    return mydesc;

}

vector< boost::dynamic_bitset<> > descriptor_extractor::extract_bitset_descriptors(const cv::Mat desc, std::vector <cv::DMatch> matches){

    boost::dynamic_bitset<> mybit(myparams_->num_bits);
    vector < boost::dynamic_bitset<> > mydesc;
    for(unsigned int i=0;i < matches.size();i++){
        cv::Mat tmp_desc = desc.row(matches[i].queryIdx);

        // decode each descriptor!!!
        for(unsigned int j=0; j < tmp_desc.rows; j++){
            for(unsigned int k=0; k < tmp_desc.cols; k++){
                int tmp = (unsigned long int)tmp_desc.at<unsigned char>(j,k);
                boost::dynamic_bitset<> mybit_tmp(myparams_->num_bits,tmp);
                //shit bits!!!
                mybit = mybit << 8;
                //store new val!!
                mybit = mybit | mybit_tmp;
            }
        }

        //cout << desc.row(matches[i].queryIdx) << endl << mybit  << " " <<endl;
        mydesc.push_back(mybit);
        mybit.reset();

    }

    return mydesc;

}

void descriptor_extractor::extract_bitset_descriptors_indices(const cv::Mat desc, std::vector <cv::DMatch> matches, std::vector<int> &indices, vector< boost::dynamic_bitset<> > &mydesc){

    boost::dynamic_bitset<> mybit(myparams_->num_bits);
    for(unsigned int i=0;i < matches.size();i++){
        cv::Mat tmp_desc = desc.row(matches[i].queryIdx);
        indices.push_back(matches[i].queryIdx);
        // decode each descriptor!!!
        for(unsigned int j=0; j < tmp_desc.rows; j++){
            for(unsigned int k=0; k < tmp_desc.cols; k++){
                int tmp = (unsigned long int)tmp_desc.at<unsigned char>(j,k);
                boost::dynamic_bitset<> mybit_tmp(myparams_->num_bits,tmp);
                //shit bits!!!
                mybit = mybit << 8;
                //store new val!!
                mybit = mybit | mybit_tmp;
            }
        }

        //cout << desc.row(matches[i].queryIdx) << endl << mybit  << " " <<endl;
        mydesc.push_back(mybit);
        mybit.reset();

    }

    return;

}
