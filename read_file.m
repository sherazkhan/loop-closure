function[file_exists,image_index,internal_index,start_img_index,time_pipeline,vocabulary_size,key_pts ...
    ,new_words,old_words,image_hypothesis,normalization_const,hypothesis_index,probability] = read_file(directory)

%read file
fid = fopen(directory,'r');

if fid == -1
    disp('Specified file does not exist!!.. check directory');
    file_exists = 0;
    image_index = 0;
    internal_index = 0;
    start_img_index = 0;

    %% store timer values
    time_pipeline = 0;
    vocabulary_size = 0;
    key_pts = 0;
    new_words = 0;
    old_words = 0;
    image_hypothesis = 0;
    normalization_const = 0;

    hypothesis_index = 0;
    probability = 0;

    return;
end

file_exists = 1;

data = textscan(fid,'%f');

row_index = 1;
elements_per_row = 59;
col_index = 1;

num_rows = fix(size(data{1},1)/elements_per_row);

%data_time_index(num_rows,elements_per_row) = 0;

for i=1:size(data{1},1)
    data_time_index(row_index,col_index) = data{1}(i,1);
    if(mod(i,elements_per_row) == 0)
        row_index = row_index + 1;
        col_index = 0;
    end
    col_index = col_index + 1;
end

%% store information in different vectors
image_index = data_time_index(:,1:2);
internal_index = data_time_index(:,3);
start_img_index = data_time_index(:,4:5);

%% store timer values
time_pipeline = data_time_index(:,6:13);
vocabulary_size = data_time_index(:,14);
key_pts = data_time_index(:,15);
new_words = data_time_index(:,16);
old_words = data_time_index(:,17);
image_hypothesis = data_time_index(:,18);
normalization_const = data_time_index(:,19);

hypothesis_index = data_time_index(:,20:39);
probability = data_time_index(:,40:59);



