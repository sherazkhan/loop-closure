//
// Add all includes here!!!

// for timing the parts of the system!!!
#include <sys/time.h>

//includes for wrappers!! and parameter interface!!
#include "parameters.h"
#include "getinput.h"
#include "feature_extractor.h"
#include "descriptor_extractor.h"
#include "descriptor_matcher.h"
#include "visualize_matches.h"

// include for vocab!!!
#include "vocabulary.h"

// include for loop closure!!!
#include "loop_closure.h"

// library for plotting
#include <matplotpp/matplotpp.h>

#include <iostream>
#include <cmath>
#include "discpp.h"

// include for maintaining statistics and writing to files!!
#include "stats.h"

double timediff(const timeval& start, const timeval& stop){
  return (stop.tv_sec - start.tv_sec) + 1.0e-6 *(stop.tv_usec - start.tv_usec);
}


int main(int argc,char* argv[]){

    timeval start;
    timeval stop;

    // timer struct
    timers_val timers;

    // instantiate the class of parameters
    params *myparams = new params();

    // the class that reads the input data!!!
    cv::Mat curr_image, next_image;
    get_input *myinput = new get_input(myparams);
    myinput->read_consecutive_images(curr_image,next_image);

    // class for feature extractor
    feature_extractor *myfeatureext = new feature_extractor(myparams);

    // class for descriptors
    descriptor_extractor *mydescext = new descriptor_extractor(myparams);

    // descriptor matcher class
    descriptor_matcher *mydescmatcher = new descriptor_matcher(myparams);

    // vocabulary storage!!!...this needs to be really efficient..
    myvocabulary *myvocab = new myvocabulary(myparams);

    // generate class instance that will check loop closure
    loop_closure *myloopclosure = new loop_closure(myparams,myvocab);

    // class that maintains and plots statistics
    stats *mystats = new stats(myparams,myvocab,myloopclosure);

    // visualization class!!!
    visualize_matches *visualize = new visualize_matches(myparams);


    while(!myinput->get_end_input()){


        cout << " --------------- " <<endl;
        cout << " Vocabulary Time index " << myvocab->get_time_index() << " ----- Data base time index " << myparams->start_index_img_ + myvocab->get_time_index() <<endl;
        cout << " ---- Current time index: " << myparams->curr_index_img_-2 <<endl;
        cout << " --------------- " <<endl;

        // feature extractor class
        //class for feature extraction
        std::vector<cv::KeyPoint> curr_keypts,next_keypts;
        timers.keypoint_extractor_time_curr = myfeatureext->extract_features(curr_image,curr_keypts);
        timers.keypoint_extractor_time_next = myfeatureext->extract_features(next_image,next_keypts);

        // descriptor class
        cv::Mat curr_desc, next_desc;
        timers.descriptor_extractor_time_curr = mydescext->extract_descriptors(curr_image,curr_keypts,curr_desc);
        timers.descriptor_extractor_time_next = mydescext->extract_descriptors(next_image,next_keypts,next_desc);

        gettimeofday(&start, NULL);  // start timer
        std::vector <cv::DMatch> good_matches = mydescmatcher->match_descriptors(curr_desc,next_desc);
        gettimeofday(&stop, NULL);  // stop timer
        timers.descriptor_matcher_time = timediff(start, stop);
        cout << " --------------- " <<endl;
        cout << " Time to match descriptors " << timers.descriptor_matcher_time << " s " <<endl;
        cout << " --------------- " <<endl;


        // extract bitset decsriptors to store in vocab!!!
        std::vector < boost::dynamic_bitset<> > currdesc = mydescext->extract_bitset_descriptors(curr_desc,good_matches);

        // cluster descriptors in the same image to find similar words!!
        gettimeofday(&start, NULL);  // start timer
        myvocab->cluster(currdesc);
        gettimeofday(&stop, NULL);  // stop timer
        timers.cluster_descriptors_time = timediff(start, stop);
        cout << " --------------- " <<endl;
        cout << " Time to cluster descriptors " << timers.cluster_descriptors_time << " s " <<endl;
        cout << " --------------- " <<endl;


        // assign words to vocabulary!!
        gettimeofday(&start, NULL);  // start timer
        myvocab->assign_BOW_index();
        gettimeofday(&stop, NULL);  // stop timer
        timers.assign_BOW_time = timediff(start, stop);
        cout << " --------------- " <<endl;
        cout << " Time to assign clusters to BOW in vocabulary " << timers.assign_BOW_time << " s " <<endl;
        cout << " --------------- " <<endl;


        // loop closure testing !!!...
        gettimeofday(&start, NULL);  // start timer
        myloopclosure->calculate_likelihood();
        gettimeofday(&stop, NULL);  // stop timer
        timers.loop_closure_check_time = timediff(start, stop);
        cout << " --------------- " <<endl;
        cout << " Time to calculate likelihood " << timers.loop_closure_check_time << " s " <<endl;
        cout << " --------------- " <<endl;

        //update vocabulary!!!
        gettimeofday(&start, NULL);  // start timer
        myvocab->update_vocabulary();
        gettimeofday(&stop, NULL);  // stop timer
        timers.update_vocabulary_time = timediff(start, stop);
        cout << " --------------- " <<endl;
        cout << " Time to update vocabulary " << timers.update_vocabulary_time << " s " <<endl;
        cout << " --------------- " <<endl;


        // visualizer class
        if(!myparams->visualize_){
            visualize->display_matches(curr_image,next_image,curr_keypts,next_keypts,good_matches);
        }


        //mystats->plot_graphs();
        mystats->write_file(timers);

        //getchar();

        // adjust variables for next loop!!!
        myvocab->update_variables();
        next_image.copyTo(curr_image);
        myinput->read_curr_image(next_image);
        cout << endl <<endl;

        getchar();
        // sleep for 200 ms and then process further!!
        //usleep(200000);



    }


    return true;
}
