function []=write_file(directory,init_ts,num_hypothesis,hypo_index,prob_index)


%{
%%% PREVIOUS CODE!!!!
fid = fopen(directory,'w');

%%% total hypothesis
fprintf(fid,'%d',size(num_hypothesis,1));

%{
total_sum = 0;
for i=1:size(num_hypothesis,1)
    if(num_hypothesis(i,1) >=20)
        total_sum = total_sum + 20;
    else if(num_hypothesis(i,1) < 20)
            total_sum = total_sum + num_hypothesis(i,1);            
        end
        
    end
end
fprintf(fid,'%d',total_sum);
%}

fprintf(fid,'\n');


for i=1:size(init_ts,1)
    [row col] = max(prob_index(i,:));
    for j=col:col
        fprintf(fid,'%d',init_ts(i));
        fprintf(fid,' ');
        fprintf(fid,'%6.6f',init_ts(i));
        fprintf(fid,' ');
        fprintf(fid,'%d',hypo_index(i,j));       
        fprintf(fid,' ');
        fprintf(fid,'%6.6f',hypo_index(i,j));       
        fprintf(fid,'\n');       
    end
end

fclose(fid);
%}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Operation of filtering matches


diff = 10;
status(size(num_hypothesis,1)) = 0;

for i=1:size(init_ts,1)
    if(i==1)
        status(i) = 1;
        continue;
    end
    
    if(i > 1)
        
        if(status(i-1) == 1)
            ts_prev_image = init_ts(i-1);
            ts_image = init_ts(i);
            [row col] = max(prob_index(i-1,:));
            ts_prev_hypo = hypo_index(i-1,col);
            [row col] = max(prob_index(i,:));
            ts_hypo = hypo_index(i,col);
        else
            [val_row val_col] = find(status(1:i-1) == 1);
            col = max(val_col);
            ts_prev_image = init_ts(col);
            ts_image = init_ts(i);
            [row2 col2] = max(prob_index(col,:));
            ts_prev_hypo = hypo_index(col,col2);
            [row2 col2] = max(prob_index(i,:));
            ts_hypo = hypo_index(i,col2);
        end
        if( (( abs(ts_image - ts_prev_image) < diff) && ( abs(ts_hypo - ts_prev_hypo) < diff)) ...
            || (( abs(ts_image - ts_prev_image) > diff) && ( abs(ts_hypo - ts_prev_hypo) > diff)) ...
            )
            status(i) = 1;
        end
    end
end


%status
total_hypo = find(status == 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fid = fopen(directory,'w');

%%% total hypothesis
fprintf(fid,'%d',size(total_hypo,2));
fprintf(fid,'\n');


for i=1:size(status,2)
    
    if(status(i) == 0)
        continue;
    end
    
    [row col] = max(prob_index(i,:));
    for j=col:col
        fprintf(fid,'%d',init_ts(i));
        fprintf(fid,' ');
        fprintf(fid,'%6.6f',init_ts(i));
        fprintf(fid,' ');
        fprintf(fid,'%d',hypo_index(i,j));       
        fprintf(fid,' ');
        fprintf(fid,'%6.6f',hypo_index(i,j));       
        fprintf(fid,'\n');       
    end
end

fclose(fid);

