// the class that stores all statistics!!!

// includes to deal with string and writing files!!
#include <sstream>
#include <fstream>

struct timers_val{
    double keypoint_extractor_time_curr;
    double keypoint_extractor_time_next;
    double descriptor_extractor_time_curr;
    double descriptor_extractor_time_next;
    double descriptor_matcher_time;
    double cluster_descriptors_time;
    double assign_BOW_time;
    double loop_closure_check_time;
    double update_vocabulary_time;
};


class stats{

public:
    stats();
    stats(params *myparams, myvocabulary *myvocab, loop_closure *myloopclosure):myparams_(myparams),myvocab_(myvocab), myloopclosure_(myloopclosure){
        file_directory_ = myparams_->write_file_directory_ + myparams_->file_name_;
        main_title_ = "Loop closure statistics";
        dislin_plotter_.page(1800,1800);
        dislin_plotter_.sclmod("FULL");
        dislin_plotter_.metafl ("cons");
        dislin_plotter_.scrmod ("revers");
        dislin_plotter_.window(100,100,700,700);
    }
    void plot_graphs();
    void write_file(timers_val timers);

private:
    std::vector<int> image_hypothesis;
    std::vector<double> prob_distribution;
    std::vector<double> occurances;
    std::string file_directory_;
    params *myparams_;
    loop_closure *myloopclosure_;
    myvocabulary *myvocab_;
    Dislin dislin_plotter_;
    const char   *main_title_;
    std::ofstream myfile_;
};

void stats::write_file(timers_val timers){

    // get data to write to the file

    // get time index
    int start_image_index = myparams_->start_index_img_;


    //int image_index = myparams_->start_index_img_ + myvocab_->get_time_index();
    // for city centre and LIP6
    int image_index = myparams_->curr_index_img_-2;

    // for Malaga data set!!!!!!!
    //int image_index = myparams_->curr_index_img_;

    // get time index of vocab
    int time_index =  myvocab_->get_time_index();

    // get vocabulary data!!
    int vocab_size = myvocab_->get_size_vocabulary();
    int num_features_found = myvocab_->get_size_clustered_desc();
    int num_new_words = myvocab_->get_num_new_words();
    int num_old_words = myvocab_->get_num_old_words();

    // get loop closure data!!
    std::vector<int> image_hypothesis = myloopclosure_->get_image_hypothesis();
    std::vector<double> myprob_dist = myloopclosure_->get_probability_dist();
    double normalization_const = myloopclosure_->get_normalization_const();

    myfile_.open(file_directory_.c_str(),std::ios::app);
    if(myfile_.is_open()){
        // preliminary data
        myfile_ << image_index << " "  << myparams_->curr_index_img2_ << " " << time_index << " " << start_image_index << " " << myparams_->start_index_img2_<< " ";

        // timer values
        myfile_ << timers.keypoint_extractor_time_curr << " " << timers.keypoint_extractor_time_next
                << " " << timers.descriptor_extractor_time_curr << " " << timers.descriptor_extractor_time_next
                << " " << timers.cluster_descriptors_time << " " << timers.assign_BOW_time
                << " " << timers.loop_closure_check_time << " " << timers.update_vocabulary_time << " ";

        // write vocabulary data
        myfile_ << vocab_size << " " << num_features_found << " " << num_new_words << " " << num_old_words << " ";

        // write loop closure data!!
        myfile_ << image_hypothesis.size() << " " << normalization_const << " ";

        if(image_hypothesis.size() < myparams_->max_loop_hypothesis_file_){

            for(unsigned int i=0;i < image_hypothesis.size();i++){
                if(myparams_->dataset_ == "Malaga"){
                    myfile_ << image_hypothesis[i] << " ";
                }
                else{
                    myfile_ << start_image_index + image_hypothesis[i] << " ";
                }
            }

            // fill in remaining slots with zeros
            int remaining = myparams_->max_loop_hypothesis_file_ - image_hypothesis.size();

            for(unsigned int i=0;i < remaining;i++){
                myfile_ << 0 << " ";
            }

            // do the same with probability dist
            for(unsigned int i=0;i < myprob_dist.size();i++){
                myfile_ << myprob_dist[i] << " ";
            }
            for(unsigned int i=0;i < remaining;i++){
                myfile_ << 0 << " ";
            }

        } // loop closure hypothesis is greater than the number allowed to store in file ...
        else if(image_hypothesis.size() >= myparams_->max_loop_hypothesis_file_){

            for(unsigned int i=0;i < myparams_->max_loop_hypothesis_file_;i++){
                if(myparams_->dataset_ == "Malaga"){
                    myfile_ << image_hypothesis[i] << " ";
                }
                else{
                    myfile_ << start_image_index + image_hypothesis[i] << " ";
                }
            }

            // do the same with probability dist
            for(unsigned int i=0;i < myparams_->max_loop_hypothesis_file_;i++){
                myfile_ << myprob_dist[i] << " ";
            }
        }

        myfile_ << endl;

    }
    myfile_.close();
    return;
}

void stats::plot_graphs(){

   if( (myparams_->plot_prob_distribution_) && (myvocab_->get_time_index() > myparams_->temporal_dist_) ){

        std::vector<int> image_hypothesis = myloopclosure_->get_image_hypothesis();
        std::vector<double> myprob_dist = myloopclosure_->get_probability_dist();

        if(image_hypothesis.size() == 0 || myprob_dist.size() == 0 || image_hypothesis.size() > 15){
            cout << " No image hypothesis and probability values or too many hypothesis " <<endl;
            return;
        }

        dislin_plotter_.disini ();
        dislin_plotter_.pagera ();
        dislin_plotter_.complx ();
        dislin_plotter_.titlin (main_title_, 1);

        double *total_hypothesis = new double[image_hypothesis.size()];
        double *prob_dist = new double[image_hypothesis.size()];
        double *minpts = new double[image_hypothesis.size()];


        // fill with data!!!
        for(unsigned int i=0;i < image_hypothesis.size();i++){
            total_hypothesis[i] = (double) i;
            minpts[i] = 0.0;
            prob_dist[i] = 0.0;
        }

        for(unsigned int j=0; j< image_hypothesis.size();j++){
            prob_dist[image_hypothesis[j]] = myprob_dist[j];
            const int m = image_hypothesis[j];
            std::ostringstream s;
            s << m;
            const std::string mystring(s.str());
            dislin_plotter_.mylab(mystring.c_str(),j+1,"x");

        }

        for(unsigned int i=0;i<image_hypothesis.size();i++){
            cout << total_hypothesis[i] << "  " << minpts[i] << "  " << prob_dist[i] <<endl;
        }

        const int size_hypothesis = image_hypothesis.size();
        std::string index = " Image index at time index ";
        std::ostringstream s;
        s << myvocab_->get_time_index();
        const std::string time_index(s.str());
        index += time_index;
        dislin_plotter_.name(index.c_str(),"x");
        dislin_plotter_.name("Probability","y");
        dislin_plotter_.labels("mylab","x");
        dislin_plotter_.labpos("ticks","x");
        dislin_plotter_.shdpat (3);
        dislin_plotter_.graf   (0, size_hypothesis, 0, 1, 0.0, 1.0, 0.0, 0.1);
        dislin_plotter_.color  ("blue");
        dislin_plotter_.bars   (total_hypothesis, minpts, prob_dist, size_hypothesis);
        dislin_plotter_.color  ("white");
        dislin_plotter_.title  ();
        dislin_plotter_.winmod("NONE");
        dislin_plotter_.endgrf ();
        dislin_plotter_.disfin ();

        delete[] total_hypothesis;
        delete[] prob_dist;
        delete[] minpts;

    }

}



