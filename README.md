# README #

This repository contains the code for loop closure detection in SLAM (Simultaneous Localization & Mapping) using a binary vocabulary 
which is generated in an online, incremental manner. The details of the algorithm can be found in the following paper:

Khan, Sheraz, and Dirk Wollherr. "IBuILD: Incremental bag of Binary words for appearance based loop closure detection." 
, IEEE International Conference on Robotics and Automation (ICRA), 2015.

### How do I get set up? ###
The code depends on Boost (version 1.54 -- boost::bitset is essential), Eigen 2 as well as OpenCV.

In addition, the code depends on the BRISK descriptor which can be downloaded from here:

http://wp.doc.ic.ac.uk/sleutene/software/

In case of any questions, kindly contact sheraz@lsr.ei.tum.de