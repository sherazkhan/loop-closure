clc
clear all
close all

display_graphs = 0;

dataset='Malaga';

directory = '/home/sheraz/SLAM/scene_flow/src/results/city_centre/loop_closure_newformat_stats_citycenter_all_brisk_48_36.txt';

if(strcmpi(dataset,'Malaga'))
    directory = '/home/sheraz/SLAM/scene_flow/src/results/malaga/loop_closure_newformat_stats_malaga_all_brisk_130_30.txt';
end


[file_exists,image_index,internal_index,start_img_index,time_pipeline,vocabulary_size,key_pts ...
    ,new_words,old_words,image_hypothesis,normalization_const, ...
    hypothesis_index,probability] = read_file(directory);


directory = '/home/sheraz/SLAM/scene_flow/src/results/new_college/matlab_data/';
directory_write = '/home/sheraz/SLAM/scene_flow/src/results/groundtruth/Malaga6L_matches_a0.3_f1_k3_my.txt';


%% plot data
if(display_graphs)
    plot(1:size(image_index,1),image_index) 
    title('Image index')
    figure;
    plot(1:size(vocabulary_size,1),vocabulary_size)
    title('Vocabulary size')
    figure;
    plot(1:size(key_pts,1),key_pts)
    title('Number of key points detected')
    figure;
    plot(1:size(new_words,1),new_words)
    title('Number of new words detected')
    figure;
    plot(1:size(old_words,1),old_words)
    title('Number of old words detected')
    figure;
    plot(1:size(image_hypothesis,1),image_hypothesis)
    title('Number of hypothesis at each time instant')
end

if(strcmpi(dataset,'Lip6'))
    mask(1:388,1:388) = 0;
    myindexes = find(image_hypothesis > 0);
    img_index = image_index(myindexes,1);
    num_hypothesis = image_hypothesis(myindexes,:);
    hypo_index = hypothesis_index(myindexes,:);
    
    for i=1:size(img_index,1)
        hypo = num_hypothesis(i,1);
        if(hypo >20)
            hypo = 20;
        end
        
        for j=1:hypo
            mask(img_index(i)+1,hypo_index(i,j)+1) = 1;
            
        end
    end
    
    imshow(mask);

    
end

if(~strcmpi(dataset,'Malaga') && ~strcmpi(dataset,'NewCollege'))
    myindexes = find(image_hypothesis > 0);
    init_ts = internal_index(myindexes,:);
    num_hypothesis = image_hypothesis(myindexes,:);
    hypo_index = hypothesis_index(myindexes,:);
    prob_index = probability(myindexes,:);
    write_file(directory_write,init_ts,num_hypothesis,hypo_index,prob_index);
end

if(strcmpi(dataset,'Malaga'))
    myindexes = find(image_hypothesis > 0);
    init_ts = internal_index(myindexes,:);
    num_hypothesis = image_hypothesis(myindexes,:);
    hypo_index = hypothesis_index(myindexes,:);
    prob_index = probability(myindexes,:);
    write_file_malaga(directory_write,image_index,start_img_index,init_ts,num_hypothesis,hypo_index,prob_index);
end


if(strcmpi(dataset,'NewCollege'))
    myindexes = find(image_hypothesis > 0);
    init_ts = internal_index(myindexes,:);
    num_hypothesis = image_hypothesis(myindexes,:);
    hypo_index = hypothesis_index(myindexes,:);
    prob_index = probability(myindexes,:);
    write_file_newcollege(directory_write,init_ts,num_hypothesis,hypo_index,prob_index);
end

%%%%%% write a seperate file format for malaga!!


%% save all files!!!
if(file_exists)
    save(strcat(directory,'image_index.mat'),'image_index')

    save(strcat(directory,'internal_index.mat'),'internal_index')

    save(strcat(directory,'start_img_index.mat'),'start_img_index')

    save(strcat(directory,'time_pipeline.mat'),'time_pipeline')

    save(strcat(directory,'vocabulary_size.mat'),'vocabulary_size')

    save(strcat(directory,'key_pts.mat'),'key_pts')

    save(strcat(directory,'new_words.mat'),'new_words')

    save(strcat(directory,'old_words.mat'),'old_words')

    save(strcat(directory,'image_hypothesis.mat'),'image_hypothesis')

    save(strcat(directory,'normalization_const.mat'),'normalization_const')

    save(strcat(directory,'hypothesis_index.mat'),'hypothesis_index')

    save(strcat(directory,'probability.mat'),'probability')
end

