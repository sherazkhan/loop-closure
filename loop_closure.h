/////

// The class that does all the work!!!
// checks for loop closure!!!

/////


class loop_closure{
public:
    loop_closure(params *myparams,myvocabulary *myvocab):myparams_(myparams),myvocab_(myvocab),normalization_const_(0.0){
    }
    ~loop_closure();
    void calculate_likelihood();
    double evaluate_likelihood(std::vector<int> word_indexes,const std::vector<int>* old_words, int &matches);

    // returns the image hypothesis at current time index
    std::vector<int> get_image_hypothesis(){
        return image_hypothesis_;
    }

    // return probability distribution at current time index
    std::vector<double> get_probability_dist(){
        return probability_dist_;
    }

    double get_normalization_const(){
        return normalization_const_;
    }

private:
    std::vector<int> image_hypothesis_;
    std::vector<double> probability_dist_;
    std::vector<double> occurances_;
    double normalization_const_;
    params *myparams_;
    myvocabulary *myvocab_;
};

double loop_closure::evaluate_likelihood(std::vector<int> word_indexes,const std::vector<int>* old_words, int &matches){

    // get entire vocabulary!!!
    const std::vector < boost::dynamic_bitset<> >* tmp_vocab = myvocab_->get_visual_vocabulary();

    // get occurances!!!
    const std::vector<double>* occurances =  myvocab_->get_visualword_occurances();

    // ger number of new words !!!
    int num_new_words = myvocab_->get_num_new_words();

    // use word indexes to get the descriptor from vocabulary!!
    double matched_weights = 0.0;
    double total_weight = 0.0;

    int count_old_matches = 0;
    double count_occurances = 0.0;
    for(unsigned int i=0;i < old_words->size();i++){
        for(unsigned int j=0;j < word_indexes.size();j++){
            if(old_words->at(i) == word_indexes[j]){
                cout << " Indexes matched " << old_words->at(i) << " " << word_indexes[j] << " with weight " <<  occurances->at(word_indexes[j]) << endl;
                matched_weights += 1/(occurances->at(word_indexes[j]));
                //matched_weights += log(occurances->at(word_indexes[j]) + 1);
                count_old_matches++;
                count_occurances += occurances->at(word_indexes[j]);
            }
        }
    }

    matches = count_old_matches;
    matched_weights = matched_weights*count_old_matches;

    for(unsigned int j=0;j < word_indexes.size();j++){
        total_weight += 1/(occurances->at(word_indexes[j]));
        //total_weight += log(occurances->at(word_indexes[j]) + 1);
    }

    total_weight = total_weight*word_indexes.size();
    total_weight = total_weight + num_new_words;

    double likelihood = matched_weights/total_weight;

    // get max occurances to weight accordingly!!!
    //double max_occurances = *std::max_element(occurances->begin(),occurances->end());

    return likelihood;

}

void loop_closure::calculate_likelihood(){

    int time_index = myvocab_->get_time_index();

    if(time_index < myparams_->temporal_dist_){
        cout << " Can not check loop closure due to temporal constraint " <<endl;
        return;
    }

    // get data from the vocab class!!!
    const DynamicSparseMatrix<bool,ColMajor> *myinverse_index_zi  = myvocab_->return_inverse_index_zi();
    const DynamicSparseMatrix<bool,ColMajor> *myinverse_index_iz  = myvocab_->return_inverse_index_iz();

    // get index of all old words to calculate the likelihood!!!
    const std::vector<int>* old_words = myvocab_->get_old_words_index();

    std::vector<int> image_hypothesis;
    // populate the hypothesis of images to test against
    for(unsigned int i=0; i< old_words->size();i++){
        for(sparse_it it(*myinverse_index_iz,old_words->at(i));it;++it){
            //cout << " Value " << it.value() << " Row number " << it.row() << " Column number " << it.col() << " Index in Matrix " << it.index() <<endl; // inner index, here it is equal to it.row()
            int image_index = it.row();

            if(image_index < (time_index - myparams_->temporal_dist_)){ // check for time index constraint
                bool repeat = false;
                for(unsigned int j=0;j< image_hypothesis.size();j++){
                    if(image_hypothesis[j] == image_index)
                        repeat = true;
                }
                if(!repeat)
                    image_hypothesis.push_back(image_index);
            }
        }
    }

    if(image_hypothesis.size() == 0){
        cout << " No possible hypothesis found!!!" << endl;
        image_hypothesis_ = image_hypothesis;
        probability_dist_.clear();
        normalization_const_ = 0.0;
        return;
    }

    // sort
    std::sort (image_hypothesis.begin(), image_hypothesis.end());

    // remove multiple instances
    std::vector<int>::iterator it;
    it = std::unique (image_hypothesis.begin(), image_hypothesis.end());

    // resize vector -- this gives us the list of hypothesis to check against!!!!
    image_hypothesis.resize( std::distance(image_hypothesis.begin(),it) );

    std::vector<double> probability_dist;
    double normalization = 0.0;

    bool reject = false;
    /// perform all amazing likelihood calculations here!!!!
    for(unsigned int i=0;i< image_hypothesis.size();i++){
        std::vector<int> word_indexes;
        for(sparse_it it(*myinverse_index_zi,image_hypothesis[i]);it;++it){
            //cout << " Value " << it.value() << " Row number " << it.row() << " Column number " << it.col() << " Index in Matrix " << it.index() <<endl; // inner index, here it is equal to it.row()
            word_indexes.push_back(it.row());
        }
        cout << " Old words size " << old_words->size() <<endl;
        cout << " Num of words in curr hypo " << word_indexes.size() << endl;
        int matches = 0;
        double probability = evaluate_likelihood(word_indexes,old_words,matches);
        probability_dist.push_back(probability);
        normalization += probability;

    }

    // normalize distribution!!!
    for(unsigned int j=0;j< probability_dist.size();j++){
        double prob = probability_dist[j];
        double norm_prob = prob/normalization;
        probability_dist[j] = norm_prob;
    }

    for(unsigned int i=0;i<image_hypothesis.size();i++){
        cout << " Loop closure hypothesis of " << image_hypothesis[i] << " with database index : " << myparams_->start_index_img_ + image_hypothesis[i]  << " with probability " << probability_dist[i] << endl;
    }
    probability_dist_ = probability_dist;
    image_hypothesis_ = image_hypothesis;
    normalization_const_ = normalization;

    //getchar();
    /////...////// End likelihood calculations!!!

    //////////////////////////////////////////////////////////////////////////
    /// TO DO!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ///////////////////////////////////////////////////////////////////////////

}
