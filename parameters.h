///

// Parameter class for all magic numbers
// included in the appraoch!!!

///
#ifndef PARAMS
#define PARAMS

#include <string>
#include <iostream>

// includes for reading files!!!
#include <boost/filesystem.hpp>
#include <iterator>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>

using namespace std;

struct correspondance{
    int index1;
    int index2;
};

class params{

public:

    params():start_index_img_(2),start_index_img2_(0),end_index_img_(2474),curr_index_img2_(0),loop_closure_th_(0.2),temporal_dist_(20),max_vocab_size_(80000),matching_th_(38),read_left_img_(true),visualize_(false){
        ///// All important params are initialized
        ///// in the constructor.. which should be overwritten by reading an xml!!!!

        //start_index_img_  // starting image index
        //end_index_img_    // image indexes to read till (if they are read from a directory)
        //loop_closure_th_  // the magic threshold to check loop closure
        //temporal_dist_    // distance in appearance space around which loop closure in not checked!!!
        //max_vocab_size_
        //matching_th_      // matching threshold between images!!!

        // plotting parameters
        plot_image_hypothesis_ = true;
        plot_prob_distribution_ = true;
        plot_occurances_ = true;

        // feature detector params
        threshold = 40;  // 60 original setting with BRISK keypoint extractor!!!

        //for fast detector
        //threshold = 100;

        octaves = 4;
        end_input = false;
        scale_invariance = true;
        rotation_invariance = true;
        keypoint_extractor_ = "BRISK";
        feature_descriptor_ = "BRISK";
        surf_hessian_ = 500;
        dataset_ = "CityCentre";

        // image directory etc
        curr_index_img_ = start_index_img_;
        //directory = "/home/sheraz/Desktop/Matlab/AMAZING_Stereo_Images_Left/";
        directory = "/home/sheraz/Desktop/Matlab/City_center/";
        //directory = "/home/sheraz/Desktop/Matlab/malaga2009_parking_6L/Images_rect/";
        //directory = "/home/sheraz/Desktop/Matlab/IndoorDataSet/Images/";
        //directory = "/home/sheraz/Desktop/Matlab/OutdoorDataSet/Images/";

        write_file_directory_ = "";
        file_name_ = "loop_closure_newformat_stats_citycentre_all_brisk_40_30.txt";
        max_loop_hypothesis_file_ = 20;
        std::string expression = "CAMERA_(LEFT|RIGHT)_(\\d{10}).(\\d{6}).jpg";
        format_ = expression;

        // set number of bits of BRISK descriptor!!!
        std::string feat = "BRISK";
        if(feature_descriptor_ == feat){
            num_bits = 512;
        }

        // decide which image to read!!!
        if(read_left_img_){
            index_ = "CAMERA_LEFT";
        }
        else if(!read_left_img_){
            index_ = "CAMERA_RIGHT";
        }
        else{
            index_ = "";
        }

        img_extension_ = ".jpg";
    }

    params(int start_index_img):end_index_img_(20000),loop_closure_th_(0.2),temporal_dist_(10),max_vocab_size_(50000),matching_th_(25){
        start_index_img_ = start_index_img;
    }

    params(int start_index_img, int end_image_index):loop_closure_th_(0.2),temporal_dist_(10),max_vocab_size_(50000),matching_th_(25){
        start_index_img_ = start_index_img;
        end_index_img_ = end_image_index;
    }

    ~params();

    void write_params(){
        cout << endl;
        cout  << "-------------------Input file parameters----------------------------" <<endl;
        cout << " Parameters of the PROGRAM " <<endl;
        cout << " Starting image index: " << start_index_img_ <<endl;
        cout << " Current image index: " << curr_index_img_ <<endl;
        cout << " Final image index: " << end_index_img_ << endl;
        cout << " Directory to read from: " << directory <<endl <<endl;


        cout << "-------------------- Loop closure parameters -------------" <<endl;
        cout << " loop closure threshold: " << loop_closure_th_ <<endl;
        cout << " temporal distance for loop closure check : " << temporal_dist_ <<endl;
        cout << " Max vocabulary size : " << max_vocab_size_ <<endl <<endl;

        cout << "-------------------- Feature matching parameters -------------" <<endl;
        cout << " Feature extractor being used: " << keypoint_extractor_ <<endl;
        cout << " Feature descriptor being used: " << feature_descriptor_ <<endl;
        cout << " Feature matching threshold : " << threshold <<endl;
        cout << " Number of octaves for feature extraction : " << octaves <<endl <<endl;

        cout << "-------------------- Descriptor extraction parameters -------------" <<endl;
        cout << " Rotation invariance for descriptor : " << rotation_invariance <<endl;
        cout << " Scale invariance for descriptor : " << scale_invariance <<endl <<endl;

    }

    //////////////////////////////////
    int start_index_img_;
    int start_index_img2_;
    int curr_index_img_;
    int curr_index_img2_;
    int end_index_img_;      //change these back to integer if something weird happens!!!!!!!!!!!!!!!!!!!!!!
    int loop_closure_th_;
    int temporal_dist_;
    ///////////////////////////////////

    int max_vocab_size_;
    int matching_th_;
    char* directory;
    char* index_;
    char* img_extension_;
    std::string keypoint_extractor_;
    std::string feature_descriptor_;
    std::string write_file_directory_;
    std::string file_name_;
    std::string dataset_;
    boost::regex format_;
    int max_loop_hypothesis_file_;
    int threshold;
    int octaves;
    bool scale_invariance;
    bool rotation_invariance;
    bool read_left_img_;
    bool visualize_;
    bool end_input;
    bool plot_image_hypothesis_;
    bool plot_prob_distribution_;
    bool plot_occurances_;
    int num_bits;
    int surf_hessian_;

};


#endif //end definition
