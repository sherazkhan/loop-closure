///

// Class that reads the input images!!!

///

#include <fstream>

/// Opencv Includes!!!!!!

#include "opencv2/core/core.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace file_iterator = boost::filesystem3;

class get_input{

public:

    get_input(params *input_params):readdir_(input_params->directory),directory_it_(readdir_){
        myparams = input_params;
    }

    bool get_end_input(){
        return myparams->end_input;
    }

    ~get_input();
    bool read_curr_image(cv::Mat &curr_img);
    bool read_consecutive_images(cv::Mat &curr_img, cv::Mat &next_img);
    void write_params();

private:
    params *myparams;
    file_iterator::path readdir_;
    file_iterator::recursive_directory_iterator directory_it_;
    file_iterator::recursive_directory_iterator directory_endit_;

};

#include "getinput.hpp"


