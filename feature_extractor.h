///

// Feature extractor class!!!!

///

// Opencv features---Descriptor extractors!!!
#include "opencv2/features2d/features2d.hpp"
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <brisk/brisk.h>

class feature_extractor{

public:
    feature_extractor(params *myparam,std::string feature_type):myparams_(myparam){

        if(myparams_->keypoint_extractor_ == feature_type){
            detector_ = new cv::BriskFeatureDetector(myparams_->threshold,myparams_->octaves);
        }else if(myparams_->keypoint_extractor_ == feature_type){
            detector_ = new cv::SurfFeatureDetector(myparams_->surf_hessian_);
        }else if(myparams_->keypoint_extractor_ == feature_type){
            detector_ = new cv::FastFeatureDetector(myparams_->threshold);
        }

    }

    feature_extractor(params *myparam):myparams_(myparam){

        std::string features = "BRISK";
        if(myparams_->keypoint_extractor_ == features){
            detector_ = new cv::BriskFeatureDetector(myparams_->threshold,myparams_->octaves);
        }

        features = "FAST";
        if(myparams_->keypoint_extractor_ == features){
            detector_ = new cv::FastFeatureDetector(myparams_->threshold);
        }


    }

    double timediff(const timeval& start, const timeval& stop){
      return (stop.tv_sec - start.tv_sec) + 1.0e-6 *(stop.tv_usec - start.tv_usec);
    }

    double extract_features(cv::Mat &img, std::vector<cv::KeyPoint> &keypoints){
        gettimeofday(&start, NULL);  // start timer
        if(myparams_->keypoint_extractor_ == "BRISK" || myparams_->keypoint_extractor_ == "FAST"){
            detector_->detect(img,keypoints);
        }
        else if(myparams_->keypoint_extractor_ == "SURF"){
            cv::SurfFeatureDetector detector;
            detector.detect(img,keypoints);
        }
        gettimeofday(&stop, NULL);  // stop timer
        double extraction_time = timediff(start, stop);
        cout << " --------------- " <<endl;
        cout << " Time to extract features " << extraction_time <<endl;
        cout << " --------------- " <<endl;

        return extraction_time;

    }


private:
    cv::Ptr<cv::FeatureDetector> detector_;
    params* myparams_;
    timeval start;
    timeval stop;

};
